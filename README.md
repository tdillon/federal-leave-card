# Federal Leave Cards

Miss the old **Federal Employees' Leave Record Card** provided by GEICO?

Try [this](https://tdillon.gitlab.io/federal-leave-cards)!

## Developer Info

### VS Code Plugin

You'll want the
[lit-plugin](https://marketplace.visualstudio.com/items?itemName=runem.lit-plugin)
for VS Code.

### Type Checking

To get type checking for Lit and Luxon in VS Code you can run the following
commands. NPM/Node is not needed for running or building the application, only
to make VS Code happy (which improves developer experience). If anyone knows how
to get VS Code to recognize typings for HTTP imports without Node/NPM, you'd be
my hero if you let me know

```shell
npm i lit luxon
npm pkg set type=module
```

### Upgrade Dependencies

`Lit` and `Luxon` versions are managed in the importmap defined in
`public/index.html`.
