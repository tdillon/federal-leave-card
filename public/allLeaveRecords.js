import { LitElement, html } from 'lit'
import { default as yearlyPayPeriodData } from './data.js'
import './leaveRecord.js'

export class AllLeaveRecords extends LitElement {
    render() {
        return yearlyPayPeriodData
            .sort((a, b) => b.year - a.year)
            .map(d => html`<leave-record year=${d.year}></leave-record>`)
    }
}

customElements.define('all-leave-records', AllLeaveRecords)
