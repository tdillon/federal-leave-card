import { css } from 'lit'

export default css`
    :host {
        --blue: #283468;
        --red: #C03538;
        --pink: #F2DDDC;
        color: var(--blue);
        font-family: "Gill Sans MT", Arial;
        font-weight: bold;
        break-after: page;
        display: block;
        margin: 2em auto;
        width: 10.5in;
    }

    h1 {
        color: var(--red);
    }

    table {
        border-collapse: collapse;
    }

    td {
        padding: 3px;
    }

    table,
    td {
        border: solid 1px black;
    }

    thead>tr:first-child td {
        color: white;
        background-color: var(--blue);
        font-size: 28px;
        padding: 8px 0;
    }

    thead {
        font-size: 14px;
        text-align: center;
    }

    thead tr:nth-child(3) {
        font-size: 10px;
    }

    tbody {
        font-size: 9px;
    }

    tbody tr>td:first-child {
        background-color: var(--blue);
        color: white;
        text-align: right;
    }

    tbody tr>td:nth-child(3),
    tbody tr>td:nth-child(9),
    tbody tr>td:nth-child(10),
    tbody tr>td:nth-child(16) {
        background-color: var(--pink);
    }

    tbody tr>td:nth-child(1) {
        font-family: "Roboto Mono", monospace;
        width: 2%;
    }

    tbody tr>td:nth-child(2) {
        font-family: "Roboto Mono", monospace;
        width: 8.5%;
    }

    tbody tr>td:nth-child(3),
    tbody tr>td:nth-child(4),
    tbody tr>td:nth-child(5),
    tbody tr>td:nth-child(6),
    tbody tr>td:nth-child(7),
    tbody tr>td:nth-child(8),
    tbody tr>td:nth-child(9),
    tbody tr>td:nth-child(10),
    tbody tr>td:nth-child(11),
    tbody tr>td:nth-child(12),
    tbody tr>td:nth-child(13),
    tbody tr>td:nth-child(14),
    tbody tr>td:nth-child(15),
    tbody tr>td:nth-child(16) {
        width: 3%;
    }

    tbody tr>td:nth-child(17),
    tbody tr>td:nth-child(18),
    tbody tr>td:nth-child(19),
    tbody tr>td:nth-child(20),
    tbody tr>td:nth-child(21),
    tbody tr>td:nth-child(22),
    tbody tr>td:nth-child(23),
    tbody tr>td:nth-child(24),
    tbody tr>td:nth-child(25) {
        width: 4%;
    }

    .holiday {
        background-color: var(--red);
        color: white;
        text-align: center;
        font-weight: bold;
    }`