import { LitElement, html } from 'lit'
import { DateTime } from 'luxon'
import { default as css } from './css.js'
import { default as payPeriodData } from './data.js'

export class LeaveRecord extends LitElement {

    static styles = css
    static properties = {
        year: { type: Number },
    }

    constructor() {
        super()
        this.year = new Date().getFullYear()
    }

    render() {
        const payPeriod = payPeriodData.find(p => p.year === this.year)
        if (!payPeriod) {
            return html`<h1>⚠️ INVALID YEAR: ${this.year}</h1>`
        }
        const startDay = DateTime.fromISO(payPeriod.start, { zone: 'utc' })

        return html`
<table>
    <thead>
        <tr>
            <td colspan=25>${this.year} Leave Record</td>
        </tr>
        <tr>
            <td rowspan=2 colspan=2>Leave Periods</td>
            <td colspan=14>&nbsp;</td>
            <td colspan=3>Annual Leave</td>
            <td colspan=3>Sick Leave</td>
            <td colspan=3>Other Leave</td>
        </tr>
        <tr>${[1, 2].flatMap(_ => ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'].map(day => html`
            <td>${day}</td>`))}${[1, 2, 3].flatMap(_ => ['E', 'U', 'Bal'].map(title => html`
            <td>${title}</td>`))}
        </tr>
    </thead>
    <tbody>${Array.from({ length: payPeriod.payPeriods }, (_v, i) => i + 1).map(payPeriodIdx => html`
        <tr>
            <td>${payPeriodIdx}</td>
            <td>
                ${startDay.plus({ days: 14 * (payPeriodIdx - 1) }).toLocaleString({ month: 'short', day: '2-digit' })}
                &mdash;
                ${startDay.plus({ days: 14 * (payPeriodIdx - 1) + 13 }).toLocaleString({ month: 'short', day: '2-digit' })}
            </td>${Array.from({ length: 14 }, (_v, i) => i).map(i => html`
            ${(payPeriod.holidays.find(h => h === startDay.plus({ days: 14 * (payPeriodIdx - 1) + i }).toISODate())
                ? html`<td class=holiday>H</td>` : html`<td></td>`)}`)}
            ${Array.from({ length: 9 }, (_v, i) => i).map(_ => html`<td></td>`)}
        </tr>`)}
    </tbody>
</table>`
    }
}

customElements.define('leave-record', LeaveRecord)